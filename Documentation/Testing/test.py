import unittest
from functions import *
class testadd(unittest.TestCase):
    def test_equal(self):
        self.assertEqual(add(2,2),4)
    def test_false(self):
        self.assertNotEqual(add(2,2),8)

    def test_type(self):
        self.assertEqual(int(add(2,2)), type)

    if __name__ == '__main__':
        unittest.main()
