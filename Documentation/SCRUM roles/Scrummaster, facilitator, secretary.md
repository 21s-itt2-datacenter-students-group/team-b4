# Rotation draft

*Draft week ww15:*

* **Scrummaster: Victor**
* **Facilitator: Ulrik**
* **Secretary: Nikandras**

# The roles in details:

### Scrumaster

*The requirements of Scrumaster role:*

<img src="https://scrumorg-website-prod.s3.amazonaws.com/drupal/inline-images/2017-03/ServantLeadership_0.png" width="450" height="200">


* Coaching the team members in self-management and cross-functionality
* Helping the Scrum team focus on high-value things that needs to be done
* Making sure Scrum events take place and are positive, productive and keep within the timelimit
* Planning and advising scrum implementations within the team
* Helping the Scrum Team understand the need for clear and concise Product Backlog items;


### Facilitator

*The requirements of Facilitator role*

* Controls the progross of team meeting in an objective, appreciativily and constructive way. But in a sense of reason and aware of his power.
* Takes responsibility regarding the  conclusions of meetings
* Makes sure team meeetings a relevant to study
* Make sure every team meber gets the chance to contribute activily, according to their prerequisites and potential


### Secretary


*The requirements of the Secretary role*

* Communication and correspondence
* Keeping a record of the team activities
* Making sure who is responsible for preparing the agenda at the time of: Teammetings, sprints etc.

