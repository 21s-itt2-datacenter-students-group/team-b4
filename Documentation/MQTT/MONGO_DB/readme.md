Credit goes to teachers - Nikolaj and Mathias 
For provoding code to test that the mqtt client/publisher, can send data and store it on MondgoDB


Original code for publisher and a guide how to setup/run it:

* Repo: https://gitlab.com/21s-itt2-datacenter-students-group/examples/mqtt-tests

Original code for client and a guide how to setup/run it:

* Repo: https://gitlab.com/21s-itt2-datacenter-students-group/examples/mqtt-to-db
