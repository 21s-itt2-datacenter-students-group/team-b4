# MQTT Publish Demo - with azure broker
# Publish 4 messages, with random numbers

import paho.mqtt.publish as publish
import random
import time

x = 1

while x < 5:
    ranint = random.randint(1, 10)
    ranstr = str(ranint)
    x += 1
    publish.single("rackroom/sensor", ranstr, hostname="brokerb4.northeurope.cloudapp.azure.com")
    time.sleep(1)

print("Done")
