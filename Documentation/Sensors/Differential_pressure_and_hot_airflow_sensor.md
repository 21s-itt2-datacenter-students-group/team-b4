Mass airflow sensors should only be used when the gas flowing through them is known to be free of contaminants.

Mass airflow sensors are only to be used when there is a uncontaminated flow of air that is able to pass through a filter. 
They are primarily used in vehicles with combustible fuel.

Differential Pressure sesnsor utilizes the effect of gasses and liquids expanding and contracting from changes in either pressure or temperature. This is very useful if you need a reading where there is no airflow or movement of gasses.

The main difference can be seen in the picture, as the mass airflow sensor is much more effective at low values.
The slope of the pressure sensor should remain consistent compared to the mass airflow. This means that Mass airflow sensors have a better resolution at lower values.





![Image](1Capture.JPG)
