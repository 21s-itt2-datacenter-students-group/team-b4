# Summary of test1

First test will include a DHT_11 sensor, that meassures temp/hum.
First was to make sure that the paho-libary is installed on our devices. Afterwards the publisher.py from the read.md was combined with code from a test done last week with the DHT11.
The two new python files were then tested with two team members. One teammate running the dht_publisher.py on his RPI and the other teammate running the client.py on his PC.

# Hardware

The components used for this test are the following:

* 3 jumper wires
* 1 10k resistor
* RaspberryPi
* DHT11

The schematic looks the following: 

![](https://raw.githubusercontent.com/makersdigest/T03-DHTXX-Temp-Humidity/master/img/raspi-Wiring.png)

The wires should be conneted the following way:

* Pin 1 (Power) = 5v
* Pin 2 (Signal) = GPIO pin 4
* Pin 3 (unused)
* Pin 4 (GND) = Ground
* resistor = placed between signal and power

# Code


**dht_publisher.py**

```python

import paho.mqtt.publish as publish
import random
import time
import Adafruit_DHT

DHT_SENSOR = Adafruit_DHT.DHT11
DHT_PIN = 4


while True:
    humidity, temperature = Adafruit_DHT.read(DHT_SENSOR, DHT_PIN)
    if humidity is not None and temperature is not None:
        print("Temp={0:0.1f}C Humidity={1:0.1f}%".format(temperature, humidity))
        publish.single("rackroom/sensor", "Temp={0:0.1f}C Humidity={1:0.1f}%".format(temperature, humidity), hostname="test.mosquitto.org")
        time.sleep(3)
    else:
        print("Sensor failure. Check wiring.");
    
```


**client.py**

```python

import paho.mqtt.client as mqtt

# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))

    # Subscribing in on_connect() - if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.subscribe("rackroom/sensor")

# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    print(msg.topic+" "+str(msg.payload.decode()))

# Create an MQTT client and attach our routines to it.
client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message
client.connect("test.mosquitto.org", 1883) #connects to broker

client.loop_forever() #listens on topic forever until program is closed

```
# result

The dht_publisher and client can communicate via two teammates. DHT11 is sending meassured data on topic.

# links

* https://gitlab.com/21s-itt2-datacenter-students-group/team-b4/-/blob/master/readme.md

* Credit for setting up DHT11 sensor and providing schematic: https://www.youtube.com/watch?v=D1Acq_LUC40 and https://www.youtube.com/watch?v=GsG1OClojOk

