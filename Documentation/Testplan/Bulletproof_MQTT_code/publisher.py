'''
Inspiration from http://www.steves-internet-guide.com/into-mqtt-python-client/
created by: Nikolaj Simonsen | february 2021
'''

import paho.mqtt.client as mqtt
import time
from datetime import datetime
import random
import json
import Adafruit_DHT
import serial

ser = serial.Serial('/dev/ttyACM0', 9600, timeout=1)
ser.flush()

# variables for mqqt and broker
sensor_id = 1234
client_name = 'publisher client'
broker_name = 'brokerb4.northeurope.cloudapp.azure.com'  
broker_port = 1883
topic1 = 'rackroom/sensor'
publish_interval = 2
client = mqtt.Client(client_name) #init client

# variables for dht11
DHT_SENSOR = Adafruit_DHT.DHT11
DHT_PIN = 4

# received message callback
def on_message(client, userdata, message):
    print('message received ', str(message.payload.decode('utf-8')))
    # print('message topic=',message.topic)
    # print('message qos=',message.qos)
    # print('message retain flag=',message.retain)

client.on_message=on_message # attach message callback to callback

# create payload
def build_payload(sensor_read, AQS_read, sensor_id):
    timestamp = datetime.utcnow().timestamp() # utc timestamp
    if sensor_read == None:
        pass
    else:
        payload = {'sensor_id': sensor_id, 'sensor_time': timestamp, 'temp_value': sensor_read[0:5], 'hum_value': sensor_read[7:12],'Co2_value': AQS_read[4:11], 'Tvoc_value': AQS_read[19:24]}
        return json.dumps(payload) # convert to json format

#read dht11
def read_sensor():
    
    temperature, humidity = Adafruit_DHT.read(DHT_SENSOR, DHT_PIN)
    
    if humidity is not None and temperature is not None:
        
        sensor_read = "{0:0.1f}C, {1:0.1f}%".format(temperature, humidity)
        return sensor_read
    
    else:
        pass
    
def arduino_ccs811():
    while True:
        if ser.in_waiting > 0:
            AQS_read = ser.readline().decode('utf-8').rstrip()
            
            return AQS_read        

#connecting to broker    
try: 
    print('connecting to broker')
    client.connect(broker_name, broker_port)
    print(f'connected to {broker_name} on port {broker_port} ')

except socket_error as e:
    print(f'could not connect {client_name} to {broker_name} on port {broker_port}\n {e}')
    exit(0)

# subscribe
client.loop_start() #start the loop
client.subscribe(topic1)
print(f'Subscribed to topics: {topic1}')

# publish
while(True):
    try:
       
        payload = build_payload(read_sensor(), arduino_ccs811(), sensor_id)
        if payload == None:
            pass
        else:
            print(f'Publishing {payload} to topic, {topic1}')
            client.publish(topic1, payload)        
            time.sleep(publish_interval) # wait x seconds

    except KeyboardInterrupt: #cleanup nice
        client.loop_stop() #stop the loop
        exit(0)


