# Humidity
# Capacitive Humidity Sensors
Humidity Sensors based on capacitive effect or simply Capacitive Humidity Sensors are one of the basic types of Humidity Sensors available.

They are often used in applications where factors like cost, rigidity and size are s of concern. In Capacitive Relative Humidity (RH) Sensors, the electrical permittivity of the dielectric material changes with change in humidity.

# Resistive Humidity Sensors (Electrical Conductivity Sensors)
Resistive Humidity Sensors are another important type of Humidity Sensors that measure the resistance (impedance) or electrical conductivity. The principle behind resistive humidity sensors is the fact that the conductivity in non – metallic conductors is dependent on their water content.

Working of Resistive Humidity Sensors
The Resistive Humidity Sensor is usually made up of materials with relatively low resistivity and this resistivity changes significantly with changes in humidity. The relationship between resistance and humidity is inverse exponential. The low resistivity material is deposited on top of two electrodes.

The electrodes are placed in interdigitized pattern to increase the contact area. The resistivity between the electrodes changes when the top layer absorbs water and this change can be measured with the help of a simple electric circuit.

# Temperature

***Thermocouple***

**What is it?** - a thermocouple consists of two dissimilar metals.  These two different metals are welded together at one end creating junction between them.  It is at the point where the temperature can actually be measured

**How does it work?**- When the two conjoined metal types experience any kind of temperature change, there is a very specific voltage that is created.  Based off the amount of voltage that is created, you can determine the temperature very accurately

![](https://control.com/uploads/textbooks/temp32.jpg)

***resistance temperature detector (RTD)***

***What is it?*** - An RTD is a temperature sensor which measures temperature using the resistance of a metal as temperature changes. 

***How does it work?*** - In practice, an electrical current is transmitted through a piece of metal located close to the area where temperature is measured. The resistance value can then be converted into temperature based on the characteristics of the element.
