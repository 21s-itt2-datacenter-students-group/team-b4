# Temperature

***Thermocouple***

**What is it?** - a thermocouple consists of two dissimilar metals.  These two different metals are welded together at one end creating junction between them.  It is at the point where the temperature can actually be measured

**How does it work?**- When the two conjoined metal types experience any kind of temperature change, there is a very specific voltage that is created.  Based off the amount of voltage that is created, you can determine the temperature very accurately

![](https://control.com/uploads/textbooks/temp32.jpg)


# Humidity
Thermal conductivity humidity sensors

Thermal conductivity humidity sensors measure the absolute humidity by calculating the difference between the thermal conductivity of dry air and the air, containing moisture. When the air is dry, it has larger heat capacity, in other words, the ability to absorb heat.

For example, in the desert where the air is dry, the nights are quite cold ..

https://www.youtube.com/watch?v=nxJHMAg4coI&feature=youtu.be

# Airflow

