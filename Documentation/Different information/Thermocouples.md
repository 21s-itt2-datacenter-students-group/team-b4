# Thermocouples

**What is it?** - a thermocouple consists of two dissimilar metals.  These two different metals are welded together at one end creating junction between them.  It is at the point where the temperature can actually be measured

**How does it work?**- When the two conjoined metal types experience any kind of temperature change, there is a very specific voltage that is created.  Based off the amount of voltage that is created, you can determine the temperature very accurately

**Why/When do you use a Thermocouple ?** 

* They are alot cheapter than RTD's, up to 2-3 times
* Depending on the type of thermocouple, they somtimes can have highter temperature meassures than RTD's
* In general they are considered more durable than RTD's or other meassures devices

# Documentaion links

* https://www.youtube.com/watch?v=abC-7OIwgTU&ab_channel=RSPSupply
