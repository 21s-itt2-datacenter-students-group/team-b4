resistance temperature detector (RTD)

What is it? - An RTD is a temperature sensor which measures temperature using the resistance of a metal as temperature changes. 

How does it work? - In practice, an electrical current is transmitted through a piece of metal located close to the area where temperature is measured. The resistance value can then be converted into temperature based on the characteristics of the element.
