# Minimum viable product

**Definition:**

> A version of a new product which allows a team to collect the maximum amount of validated learning about customers with the least effort

The validated learning comes in the form of whether the customers actually buys your product (customers = OME)


**Expected Benefits:**

> The primary benefit of an MVP is you can gain understanding about your customers’ interest in your product without fully developing the product. 

The sooner that a team can find out where their product will appeal to customers, the less effort and exepense then they have to spend on a product that might/will not succed in the market


**Potential cost**

> The minimum aspect of MVP encourages teams to do the least amount of work possible to useful feedback, which helps them avoid working on a product that no one wants.

