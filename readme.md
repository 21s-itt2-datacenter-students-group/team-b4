# Team B4

**ITT2 datacenter project members:**

Finn Jensen

Bogdan-Alexandru Vanghelie

Mehrang Pezeshki

Victor Bebawy

Matas Krikstaponis

Nikandras Kinderis

Ulrik Vinther-Jensen

Mathias Andersen


# Project plan

- https://gitlab.com/21s-itt2-datacenter-students-group/team-b4/-/blob/master/Project%20Plan.md

# premortem 

* [Premortem Links](https://gitlab.com/21s-itt2-datacenter-students-group/team-b4/-/blob/master/Pre-mortem)
# System brainstorm

* [Link to Brainstorm](Untitled_Diagram_1_.jpg)
# Block Diagram

![Blockdiagram v.1](blockdiagram.jpg)

# Use Case 
To measure temperature, humidity, airflow, Co2 and TVOC. To ensure better human conditions in data center environment.

# Requirements draft / sensor draft

* 3 sensors. Measuring temperature, humidty, airflow, Co2 and TVOC
* Node red dashboard, showing graph over time with sensors(OME needs)
* Extra equipment for airflow sensor (CO2 tubes, vacum pipe)

# MQTT introduction solution code (week6.ex2)

* [Subscriber](https://gitlab.com/21s-itt2-datacenter-students-group/team-b4/-/blob/master/Documentation/MQTT/client.py)

* [Publisher](https://gitlab.com/21s-itt2-datacenter-students-group/team-b4/-/blob/master/Documentation/MQTT/publish.py)

# OME students research

**Q1: What is the learning goals of the education ?**

It’s a broad education that contains:
Thermal machines and systems
Electrical and electronic machinery, plant and equipment
Process analysis and automation
Management, finance and safety
Professional internship for 3 months
Bachelor project
Electives - which is the project we have with you.

http://www.fms.dk/om-uddannelsen/

**Q2: What educational level in the qualification framework is the OME education ?**

The education is level 6

https://www.ug.dk/uddannelser/professionsbacheloruddannelser/tekniskeogteknologiskeudd/maritimeuddannelser/maskinmester

**Q3: What courses are included in the education ?**

In the electives we get CDCP.

**Q4: What kind of jobs does OME’s occupy ?**

Jobs like:
Project manager
Sale engineer
Energy consultant

https://www.ug.dk/uddannelser/professionsbacheloruddannelser/tekniskeogteknologiskeudd/maritimeuddannelser/maskinmester

**Q5: What kind of documentation models does OME’s use ?**

*Pending*

**Q6: What challenges can we experience in our communication with OME’s**

One clear challenge is our skills in communication in English. We also have not used discord before so that might give some challenges.

**Q7: Other relevant questions that you would like knowledge about**

What do you expect us to bring to this project, and what do you expect to bring to the project.

**Credit**

- all answers was provided by the help of OME students. Thanks to Jacob and Christian from ventilation.

# List of possible sensors (week7.ex3)

* [link] (https://gitlab.com/21s-itt2-datacenter-students-group/team-b4/-/blob/master/Documentation/Different%20information/Preparations.md)
* [link] (https://gitlab.com/21s-itt2-datacenter-students-group/team-b4/-/blob/master/Documentation/Different%20information/readhim.md)
* [link] (https://gitlab.com/21s-itt2-datacenter-students-group/team-b4/-/blob/master/Documentation/Different%20information/Thermocouples.md)
* [link] (https://www.youtube.com/watch?v=wpAA3qeOYiI)
* [link] (https://gitlab.com/21s-itt2-datacenter-students-group/team-b4/-/blob/master/Documentation/Different%20information/readthat.md)
* [link] (https://gitlab.com/21s-itt2-datacenter-students-group/team-b4/-/edit/master/Documentation/Sensors/Differential_pressure_and_hot_airflow_sensor.md)

# MQTT (week7.ex4)

**Week 7, test1 with DHT11 and MQTT. Link below provides an overview of hardware, software used in the test**

* https://gitlab.com/21s-itt2-datacenter-students-group/team-b4/-/blob/master/Documentation/Test%20plan.md

# Sensor List and possible sensors (final - to be ordered by teachers)

| sensor name   | cost  | pros  | cons  | link  |
|---|---|---|---|---|
| DHT11 | kr. 33.39 | cheap, two sensors in one | bad accuracity, but sufficient | https://dk.farnell.com/en-DK/dfrobot/dfr0067/temperature-humidity-sensor-arduino/dp/2946103?st=dht11|
| Sensirion SDP810-125 PA | kr. 276.62 | can measure accurat in airflow | quite expensiv  | https://dk.farnell.com/en-DK/sensirion/sdp810-125pa/pressure-sensor-digital-125pa/dp/2886665?ost=sdp810-125+pa&cfm=true |
| AMS CCS811 Sensor | kr. 165,42 | can measure airquality for humans| expensive | https://dk.farnell.com/en-DK/digilent/410-386/pmod-aqs-board-air-quality/dp/3003248?st=air%20quality%20sensor |


# Scrum master, facilitator, secretary on rotation (week 8)

- https://gitlab.com/21s-itt2-datacenter-students-group/team-b4/-/blob/master/Documentation/SCRUM%20roles/Scrummaster,%20facilitator,%20secretary.md

# Milestones

- https://gitlab.com/21s-itt2-datacenter-students-group/team-b4/-/milestones

# Data persistence using MongoDB exercise recreation steps and troubleshooting tips (week9.ex1)

* [Link](https://gitlab.com/21s-itt2-datacenter-students-group/team-b4/-/tree/master/Documentation/MQTT/MONGO_DB)

# POC

* [Proof of concept Teamb4](https://youtu.be/s3fDAL1sdAc)

# Link to subgroup projects

* [MQTT client to publisher](https://gitlab.com/21s-itt2-datacenter-students-group/team-b4-group/mqtt)

* [MQTT to DB](https://gitlab.com/21s-itt2-datacenter-students-group/team-b4-group/mqtt_to_db)

* [documentation](https://gitlab.com/21s-itt2-datacenter-students-group/team-b4-group/documentation)

* [scrum](https://gitlab.com/21s-itt2-datacenter-students-group/team-b4-group/scrum-roles)

* [documentation](https://gitlab.com/21s-itt2-datacenter-students-group/team-b4-group/sensor-list-v2)

# MVP
* [Minimum Viable Product Teamb4](https://www.youtube.com/watch?v=vQ5KteSlHRo)

# List of potentional vulnerabilities
* [Link](https://gitlab.com/21s-itt2-datacenter-students-group/team-b4-group/problems-and-solutions/-/tree/master)

# Buisness research

* https://gitlab.com/21s-itt2-datacenter-students-group/team-b4-group/documentation/-/tree/master/Business%20understanding

# WebsiteB4 
* [Link](https://21s-itt2-datacenter-students-group.gitlab.io/team-b4-group/websiteb4/)

# Unit Testing B4
* [Link](https://gitlab.com/ReFresh20/unit-testing-b4)

